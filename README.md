# Rzine


### Template `Rzine`

Adapted from the corresponding `readthedown` template provided by the [rmdformats](https://juba.github.io/rmdformats/) package, fully responsive with dynamic table of contents and collapsible navigation.

![](man/figures/rzine.png)


## Features and helpers

### Features matrix


<table>
<thead>
    <tr>
    <th></th>
    <th>Responsive</th>
    <th>Dynamic TOC</th>
    <th>Dark mode</th>
    <th>Thumbnails / Lightbox</th>
    <th>Code folding</th>
    <th>Tabsets</th>
    <th>Bad joke</th>
    </tr>
</thead>
<tbody>


<td><strong>rzine</strong></td>
<td>x</td>
<td>x</td>
<td></td>
<td></td>
<td>x</td>
<td>x</td>
<td></td>



</tbody>
</table>



### Helpers

The package also provides RStudio document
templates to easily generate an empty and ready to use rmarkdown file with
several configuration directives.

It also provides the `pilltabs()` helper function, which allows to display a crosstab dynamically. 

## Installation

You can install the latest stable release from gitlab :

```r
remotes::install_gitlab("hpecout/rzine", host="https://gitlab.huma-num.fr/")
```


## Creating a new document

Just create a new `Rmd` file and add the following in your YAML preamble :

```
---
output: rzine::rzine
---
```

Within RStudio , you can also choose `File` > `New File...` > `R Markdown...`, then select `From Template`. You should then be able to create a new document from rzine template.


## Options

Depending on the features provided by the template, you can add the following options to your YAML preamble. Look at the template function help page for a valid list :

- `fig_width` : figures width, in inches
- `fig_height` : figures height, in inches
- `fig_caption` : toggle figure caption rendering
- `highlight` : syntax highlighting
- `thumbnails` : if TRUE, display content images as thumbnails
- `lightbox` : if TRUE, add lightbox effect to content images
- `gallery` : if TRUE, add navigation between images when displayed in lightbox
- `use_bookdown` : if TRUE, will use `bookdown` instead of `rmarkdown` for HTML rendering, thus providing section numbering and [cross references](https://bookdown.org/yihui/bookdown/cross-references.html).
- `embed_fonts` : if `TRUE` (default), use local files for fonts used in the template instead of links to Google Web fonts. This leads to bigger files but ensures that the fonts are available
- additional aguments are passed to the base `html_document` RMarkdown template


Example preamble :

```
---
title: "Le titre de ma fiche Rzine"
subtitle: "Un sous-titre pour ma fiche Rzine"
date: "`r Sys.Date()`"
author: 
 - name: Prénom Nom
   affiliation: Affilation
logo: "figures/featured.png"   
output:
  rzine::rzine:
    highlight: kate
    number_sections: true
bibliography: cite.bib
nocite: |
  @*
link_citations: true
---
```

## Credits

- [Magnific popup](https://dimsemenov.com/plugins/magnific-popup/) lightbox plugin
- The CSS and JavaScript for `readthedown` is adapted from the corresponding `readtheorg` theme of the [org-html-themes](https://github.com/fniessen/org-html-themes) project, which is itself inspired by the [Read the docs](https://readthedocs.org/) [Sphinx](http://sphinx-doc.org/) theme.
- JavaScript and HTML code for code folding and tabbed sections are taken from the RStudio's default `rmarkdown` HTML template.

